
package common;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Mail.xml");
		
		//MailMail mm = (MailMail) context.getBean("mailMail");
		//mm.sendMail("Yong Mook Kim", "This is text content");
		
		MailMail mm = (MailMail) context.getBean("mailMail");
		mm.sendMail("from@no-spam.com", "to@no-spam.com", "Testing123", "Testing only \n\n Hello Spring Email Sender");
		
	}
}
